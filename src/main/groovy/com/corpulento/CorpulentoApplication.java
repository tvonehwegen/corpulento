package com.corpulento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorpulentoApplication {

	public static void main(String[] args) {

		SpringApplication.run(CorpulentoApplication.class,
			args);
	}
}
