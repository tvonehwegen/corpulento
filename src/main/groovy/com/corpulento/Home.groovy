package com.corpulento

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class Home {

	@RequestMapping("/")
	def index(Model model, @RequestParam(value="name", required=false, defaultValue="Guest") String name) {
		model.addAttribute("name", name);
		model.addAttribute("pagename", "home");
		
		return "home";
	}
	
	// Login form with error
	@RequestMapping("/login-error")
	def loginError(Model model) {
		model.addAttribute("loginError", true);
		
		return "login";
	}
}