package com.corpulento.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import com.corpulento.models.Account
import com.corpulento.services.AccountService

@Controller
class BudgetController {

	@Autowired
	private AccountService accountService;

	@RequestMapping("/budget")
	String index(Model model, @RequestParam(value="name", required=false, defaultValue="Guest") def name) {
		model.addAttribute("name", name);
		model.addAttribute("pagename", "budget");

		def accounts = accountService.findAll();
		model.addAttribute("accounts", accounts);

		return "budget";
	}
}