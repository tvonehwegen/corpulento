package com.corpulento.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class LoginController {

	@RequestMapping("/login")
	String index(Model model) {
		model.addAttribute("pagename", "login");
		
		return "login";
	}
}