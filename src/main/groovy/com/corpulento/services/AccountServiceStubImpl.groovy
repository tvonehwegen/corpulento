package com.corpulento.services

import java.util.List
import org.springframework.stereotype.Service
import com.corpulento.models.Account
import com.corpulento.models.User
import com.jayway.jsonpath.internal.function.numeric.Max


@Service
class AccountServiceStubImpl implements AccountService {
	private def accounts = [
		new Account(1L, "First Account", new BigDecimal(12455.12), 1L),
		new Account(2L, "Second Account", new BigDecimal(255.01), 1L),
		new Account(3L, "Third Account", new BigDecimal(982847), 1L),
		new Account(4L, "Fourth Account", new BigDecimal(12.45), 2L),
		new Account(5L, "Fifth Account", new BigDecimal(2845), 2L)
	]

	@Override
	public List<Account> findAll() {

		this.accounts;
	}

	@Override
	public Account findById(Long id) {

		return this.posts.stream().filter( { a -> Objects.equals(a.getId(), id) });
	}

	@Override
	public Account create(Account account) {

		account.setId(this.posts.stream().mapToLong({ a -> a.getId() }).max().getAsLong + 1);
		this.accounts.add(account);

		return account;
	}

	@Override
	public Account edit(Account account) {

		def c = {
			if(Objects.equals(this.accounts.get(i).getId(), account.getId()) {
				this.accounts.set(i, account);
				return account;
			}
		}
		0.upto(this.posts.size(), c);

		throw new RuntimeException("Account not found: " + account.getId());
	}

	@Override
	public void deleteById(Long id) {

		def c = {
			if (Objects.equals(this.posts.get(i).getId(), id)) {
				//this.accounts.;
				//TODO: implement? How do we delete from posts?
			}
		}
		0.upto(this.posts.size(), c)
	}
}
