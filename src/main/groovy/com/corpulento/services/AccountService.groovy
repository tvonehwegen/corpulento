package com.corpulento.services

import com.corpulento.models.Account


interface AccountService {

	List<Account> findAll();
	Account findById(Long id);
	Account create(Account account);
	Account edit(Account post);
	void deleteById(Long id);
}
