package com.corpulento.models


class User {

	private Long num;
	private String name;

	def User(Long num, String name) {
		this.num = num;
		this.name = name;
	}

	def getNum() {
		return num;
	}
	def setNum(Long num) {
		this.num = num;
	}

	def getName() {
		return name;
	}
	def setName(def name) {
		this.name = name;
	}
}
