package com.corpulento.models

import java.math.RoundingMode
import java.text.NumberFormat

class Account {

	private Long num;
	private String id;
	private BigDecimal balance;
	private Long userNum;

	def Account(Long num, String id, BigDecimal balance, Long userNum) {
		this.num = num;
		this.id = id;
		this.balance = balance;
		this.userNum = userNum;
	}

	Long getNum() {
		return num;
	}
	def setNum(Long num) {
		this.num = num;
	}

	String getId() {
		return id;
	}
	def setId(String id) {
		this.id = id;
	}

	String getBalance() {
		return NumberFormat.getCurrencyInstance().format(balance);
	}

	def setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	Long getUserNum() {
		return userNum;
	}
	def setUserNum(Long userNum) {
		this.userNum = userNum;
	}
}
